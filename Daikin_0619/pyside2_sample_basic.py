# -*- coding: utf-8 -*-

import sys
import os
import logging
from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui

# -------maya module-------
from functools import partial
import pymel
import pymel.core as pm
import maya.standalone

sys.dont_write_bytecode = True  # pycを作らないようにします


class Form(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.setWindowTitle("Sample GUI")
        self.resize(200, 75)
        
        # レイアウトを定義
        layout = QtWidgets.QVBoxLayout()

        # QSpinBox
        self.spinBox = QtWidgets.QSpinBox()
        self.spinBox.setMinimum(1)  # 最小値の設定
        self.spinBox.setMaximum(10)  # 最大値の設定
        layout.addWidget(self.spinBox)  # レイアウトにspinBoxを追加

        # 実行ボタン
        self.button01 = QtWidgets.QPushButton("make Sphere")
        layout.addWidget(self.button01)

        # レイアウトをセット
        self.setLayout(layout)

        # signalの設定
        self.button01.clicked.connect(self.make_sphere)

    def make_sphere(self):  # sphereを作る関数
        count = int(self.spinBox.value())  # spinBoxの値を取得

        for i in range(count):  # 取得した値を1から順番にfor文で回します
            pm.sphere()  # sphereを作成
            pm.move([i * 5, i * 1, 0])  # 更にsphereの位置を変更


if __name__ == '__main__':
    form = Form()    
    form.show()
